import os
import numpy as np
import pandas as pd

from pathlib import Path

import tensorflow as tf

nvidia_gpu = tf.config.list_physical_devices('GPU')[0]
try:
    tf.config.experimental.set_memory_growth(nvidia_gpu, True)
except:
    pass

from sklearn import preprocessing
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score

from tensorflow.keras.layers import Dense, BatchNormalization, Dropout, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import regularizers
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing import image
import tensorflow as tf

from data_engineering.cnn import get_data

def cnn_model(activation='relu', 
                learning_rate=0.0001,
                epochs=200,
                batch_size=4):

    X_train, y_train, X_val, y_val, X_test, y_test = get_data(load_pickle=True)

    model = Sequential()

    model.add(Conv2D(64, kernel_size=(3,3), activation=activation, input_shape=(128, 196, 3)))
    model.add(BatchNormalization())
    model.add(Dropout(0.25))

    model.add(Conv2D(64, kernel_size=(3,3), activation=activation))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(32, kernel_size=(3, 3), activation=activation))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(32, kernel_size=(3, 3), activation=activation))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(32, kernel_size=(3, 3), activation=activation))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(96, activation=activation))
    model.add(Dropout(0.5))

    model.add(Dense(32, activation=activation))
    model.add(Dropout(0.2))
    model.add(Dense(8, activation='softmax'))

    optimizer = Adam(learning_rate=learning_rate)

    # model.save(Path('../saved/cnn.h5'))

    model.compile(optimizer=optimizer,
                loss='sparse_categorical_crossentropy',
                metrics=['accuracy'])

    model.fit(x=X_train,
            y=y_train,
            epochs=epochs,
            batch_size=batch_size,
            validation_data=(X_val, y_val)
            )

        
    model.save(Path('../saved/cnn.h5'))

    preds = model.predict(X_test)
    print('accuracy on test data: {}'.format(accuracy_score(y_test, np.argmax(preds, axis=1))))

cnn_model()