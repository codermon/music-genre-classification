from train import cnn, crnn
import fire


def train(model='cnn', **kwargs):

    if model == 'cnn':
        fire.Fire(cnn.cnn_model)
    elif model == 'crnn':
        fire.Fire(crnn.crnn_model)
    else:
        print('(Training Stage) Invalid Input')
