import numpy as np
import matplotlib.pyplot as plt
from config import AUDIO_PATH
import librosa
import os


def create_spectogram(track_id):
    filename = get_audio_path(AUDIO_PATH, track_id)
    y, sr = librosa.load(filename)
    spect = librosa.feature.melspectrogram(y=y, sr=sr, hop_length=1024, n_fft=2048)
    spect = librosa.power_to_db(spect, ref=np.max)
    return spect.T


def get_track_ids(audio_path):
    track_ids = []
    
    for _, dirnames, files in os.walk(audio_path):
        if dirnames == []:
            track_ids.extend(int(file[:-4]) for file in files)
    return track_ids

def get_audio_path(audio_path, track_id):
    track_str = '{:06d}'.format(track_id)
    return os.path.join(audio_path, track_str[:3], track_str + '.mp3')

def save_spect(df):
    for index, row in df.iterrows():
        if index <144939: continue
        try:
            spect = create_spectogram(index)
        except:
            print('Track failed: {}'.format(str(index)))
            continue
        plt.figure(figsize=(15,5))
        librosa.display.specshow(spect.T, y_axis='mel', x_axis='time')
        plt.savefig('./images/' + str(index),bbox_inches='tight',transparent=True, pad_inches=0)
        plt.close()