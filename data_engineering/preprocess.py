from data_engineering import crnn
from data_engineering import cnn

def prepare_data(model='cnn', load_pickle=True, generate_image=False):
    
    if model == 'crnn':
        crnn.get_data(load_pickle=load_pickle)
    elif model == 'cnn':
        cnn.get_data(load_pickle=load_pickle, generate_image=generate_image)
    else:
        print('Invalid Input')


