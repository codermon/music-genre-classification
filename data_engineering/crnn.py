import os
import numpy as np
import pandas as pd
import librosa
import librosa.display
from sklearn.utils import shuffle
from data_engineering.utils import *
from config import TRACKS
from pathlib import Path

def get_data(load_pickle=True):

    if not load_pickle:
        tracks = pd.read_csv(TRACKS, index_col=0, header=[0, 1])
        keep_cols = [('set', 'split'), ('set', 'subset'),('track', 'genre_top')]

        df_all = tracks[keep_cols]
        df_all = df_all[df_all[('set', 'subset')] == 'small']

        df_all['track_id'] = df_all.index

        
        df_train = df_all[df_all[('set', 'split')]=='training']
        df_val = df_all[df_all[('set', 'split')]=='validation']
        df_test = df_all[df_all[('set', 'split')]=='test']

        X_train, y_train = create_array(df_train)
        X_val, y_val = create_array(df_val)
        X_test, y_test = create_array(df_test)

        X_train_raw = librosa.core.db_to_power(X_train, ref=1.0)
        X_train = np.log(X_train_raw)

        X_val_raw = librosa.core.db_to_power(X_val, ref=1.0)
        X_val = np.log(X_val_raw)

        X_test_raw = librosa.core.db_to_power(X_test, ref=1.0)
        X_test = np.log(X_test_raw)

        np.savez(Path('../pickle/crnn_train'), X_train, y_train)
        np.savez(Path('../pickle/crnn_val'), X_val, y_val)
        np.savez(Path('../pickle/crnn_test'), X_test, y_test)
    
    else:
        try:
            npz_train = np.load(Path('../pickle/crnn_train.npz'))
            X_train = npz_train['arr_0']
            y_train = npz_train['arr_1']

            npz_val = np.load(Path('../pickle/crnn_val.npz'))
            X_val = npz_val['arr_0']
            y_val = npz_val['arr_1']

            npz_test = np.load(Path('../pickle/crnn_test.npz'))
            X_test = npz_test['arr_0']
            y_test = npz_test['arr_1']
        except:
            print("Failed to load data")
            exit()

    return X_train, y_train, X_val, y_val, X_test, y_test
   

def create_array(df):
    
    df = shuffle(df)

    dict_genres = {
                   'Electronic':1, 'Experimental':2, 'Folk':3, 'Hip-Hop':4,
                   'Instrumental':5,'International':6, 'Pop' :7, 'Rock': 8
                  }
    
    genres = []
    X_spect = np.empty((0, 640, 128))
    count = 0
    #Code skips records in case of errors
    for index, row in df.iterrows():
        try:
            count += 1
            track_id = int(index)
            genre = str(row[('track', 'genre_top')])
            spect = create_spectogram(track_id)

            # Normalize for small shape differences
            spect = spect[:640, :]
            X_spect = np.append(X_spect, [spect], axis=0)
            genres.append(dict_genres[genre])
            if count % 100 == 0:
                print("Currently processing: ", index)
        except:
            print("Couldn't process: ", count)
            continue
    y_arr = np.array(genres)
    return X_spect, y_arr