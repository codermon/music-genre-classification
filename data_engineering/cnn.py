import os
import numpy as np
import pandas as pd
from matplotlib.image import imread
import librosa
import librosa.display
from sklearn.utils import shuffle
from tensorflow.keras.preprocessing import image
from data_engineering.utils import *
from config import TRACKS
from pathlib import Path

def get_data(generate_image=False, load_pickle=True):

    if generate_image or not load_pickle:
        tracks = pd.read_csv(TRACKS, index_col=0, header=[0, 1])

        keep_cols = [('set', 'split'), ('set', 'subset'),('track', 'genre_top')]

        genres = tracks[('track', 'genre_top')].unique()
        labels = {}
        for index, genre in enumerate(genres):
            labels[genre] = index

        df_all = tracks[keep_cols]
        df_all = df_all[df_all[('set', 'subset')] == 'small']

        df_all['track_id'] = df_all.index

        
        df_train = df_all[df_all[('set', 'split')]=='training']
        df_val = df_all[df_all[('set', 'split')]=='validation']
        df_test = df_all[df_all[('set', 'split')]=='test']

        if generate_image:
            save_spect(df_train)
            save_spect(df_val)
            save_spect(df_test)
        
        if not load_pickle:
            X_train, y_train = create_array(df_train, labels)
            X_val, y_val = create_array(df_val, labels)
            X_test, y_test = create_array(df_test, labels)
            np.savez(Path('../pickle/cnn_train'), X_train, y_train)
            np.savez(Path('../pickle/cnn_val'), X_val, y_val)
            np.savez(Path('../pickle/cnn_test'), X_test, y_test)
    
    else:
        try:
            npz_train = np.load(Path('../pickle/cnn_train.npz'))
            X_train = npz_train['arr_0']
            y_train = npz_train['arr_1']

            npz_val = np.load(Path('../pickle/cnn_val.npz'))
            X_val = npz_val['arr_0']
            y_val = npz_val['arr_1']

            npz_test = np.load(Path('../pickle/cnn_test.npz'))
            X_test = npz_test['arr_0']
            y_test = npz_test['arr_1']

            
        except Exception as es:
            raise  es
            print('Failed to load data')
            exit()

    return X_train, y_train, X_val, y_val, X_test, y_test

def create_array(df, labels):
    y = []
    X = []
    count = 0
    df = shuffle(df)
    for index, row in df.iterrows():
        
        count+=1
        
        try:
            x = image.load_img(
                path=Path('../images/' + str(index) + '.png'),
                color_mode='rgb',
                target_size=(128,196)
            )
            label = labels[row[('track', 'genre_top')]]
        except:
            print('Image Load failed: {}'.format(index))
            continue
        
        X.append(np.array(x))
        y.append(label)

    print('Done')
    return np.array(X),np.array(y)
