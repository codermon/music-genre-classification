from data_engineering.preprocess import prepare_data
from train.train_model import train
import fire

if __name__ == "__main__":
    fire.Fire()